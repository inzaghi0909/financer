# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140622083856) do

  create_table "products", force: true do |t|
    t.string   "product_name"
    t.string   "real_name"
    t.string   "platform"
    t.string   "product_type"
    t.string   "nh_data"
    t.string   "yield_data"
    t.string   "yield_date"
    t.integer  "yield_data_count"
    t.string   "yield_tread"
    t.float    "yield_tread_data"
    t.string   "fund_code"
    t.string   "url"
    t.integer  "job_id"
    t.integer  "show",             default: 1
    t.integer  "sort",             default: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "products", ["platform"], name: "index_products_on_platform", using: :btree
  add_index "products", ["product_name"], name: "index_products_on_product_name", using: :btree
  add_index "products", ["product_type"], name: "index_products_on_product_type", using: :btree

  create_table "yield_data", force: true do |t|
    t.integer  "product_id"
    t.string   "yield_date"
    t.string   "yield_data"
    t.string   "nh_data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "yield_data", ["product_id"], name: "index_yield_data_on_product_id", using: :btree

end
