class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :product_name
      t.string :real_name
      t.string :platform
      t.string :product_type
      t.string :nh_data
      t.string :yield_data
      t.string :yield_date
      t.integer :yield_data_count
      t.string :yield_tread
      t.float  :yield_tread_data
      t.string :fund_code
      t.string :url
      t.integer :job_id
      t.integer :show, :default => 1
      t.integer :sort, :default => 20
      t.timestamps
    end

    add_index :products, :product_name
    add_index :products, :platform
    add_index :products, :product_type
  end
end
