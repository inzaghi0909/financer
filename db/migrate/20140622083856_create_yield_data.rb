class CreateYieldData < ActiveRecord::Migration
  def change
    create_table :yield_data do |t|
      t.integer :product_id
      t.string :yield_date
      t.string :yield_data
      t.string :nh_data

      t.timestamps
    end

    add_index :yield_data, :product_id
  end
end
