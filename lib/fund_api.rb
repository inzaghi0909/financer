require 'httparty'

module API	
	class FundAPI
		include HTTParty

		# 7日数据
		# data_type: 数据类型(年, 月)
		def self.qr_data(fund_code, data_type='y')
			response = self.get(self.data_params(fund_code, data_type)).body
			JSON.parse(response)['qrdata']
		end

		# 请求uri
		def self.data_params(fund_code, data_type, version='3.7.7')
			"http://fundex2.eastmoney.com/FundWebServices/FundDataForMobile.aspx?v=#{version}&os=ios&rg=#{data_type}&fc=#{fund_code}&t=7rnew"
		end
	end
end