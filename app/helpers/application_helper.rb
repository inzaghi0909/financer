module ApplicationHelper

	def eastmoney_fund_url fund_code
		"http://fund.eastmoney.com/#{fund_code}.html"
	end
end
