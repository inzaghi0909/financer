class Dashxxboard::ProductsController < Dashxxboard::BaseController

	before_filter :find_product, :only => [:show, :fetch, :edit, :update]

	def index
		@products = Product.hot
	end

	def show
		@product.test_fetch
	end

	def fetch
		@product.fetch
		redirect_to dashxxboard_products_path, notice: "正在更新 #{@product_name} 数据, 5秒后刷新"
	end

	def fetchAll
		SchedulerFundWorker.perform_async
		redirect_to dashxxboard_products_path
	end

	def new
		@product = Product.new
	end

	def create
		@product = Product.new(product_params)
		if @product.save
			redirect_to dashxxboard_products_path, notice: "添加理财产品#{@product.product_name}成功"
		else
			render action: :new
		end		
	end

	def edit		
	end

	def update
		if @product.update_attributes product_params
			redirect_to dashxxboard_products_path
		else
			render action: :edit
		end
	end

	private
		def product_params
			params.require(:product).permit(:product_name, :real_name, :product_type,
				:platform, :url, :fund_code, :yield_data, :yield_date, :nh_data)
		end

		def find_product
			@product = Product.find(params[:id])
		end
end
