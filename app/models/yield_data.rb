class YieldData < ActiveRecord::Base
	validates :product_id, :yield_data, :yield_date, presence: true
	validates :product_id, uniqueness: {scope: :yield_date}
	
	belongs_to :product, counter_cache: true

	default_scope { order('id desc') }
end
