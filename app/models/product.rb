class Product < ActiveRecord::Base

	validates_uniqueness_of :product_name
	validates_presence_of :product_name
	validates_presence_of :product_type
	validates_presence_of :real_name
	validates_presence_of :fund_code
	validates_presence_of :platform

	has_many :yieldDatas
    after_create :fetch_one_year

    scope :hot, -> { order('yield_data desc, yield_date desc')}

	def fetch		
		FundWorker.perform_async(false, id, fund_code, 'y')
	end

	def fetch_one_year
		FundWorker.perform_async(true, id, fund_code, 'n')
	end

	def filter_fetch_data data
		# data过滤
		data
	end

	def update_recent_yield yield_data
		yield_arr = yield_data.split(',')
		begin
			update_attributes(yield_date: yield_arr[0],
							  yield_data: yield_arr[1],				
								 nh_data: yield_arr[2])	
		rescue ActiveRecord::RecordInvalid => invalid
			p invalid.record.errors
			p '*' * 40
		end
	end
end
