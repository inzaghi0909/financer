class FundWorker
	include Sidekiq::Worker

	def perform(isInit, product_id, funcode, datatype)
		datas = API::FundAPI.qr_data(funcode, datatype)		
		product = Product.find(product_id)
		datas = product.filter_fetch_data(datas) unless isInit
		save_product_yield_data(product, datas) if product
	end

	def save_product_yield_data product, datas
		# 更新最近收益
		update_recent_yield product, datas.last

		datas.each do |data_str|
			begin	
				data = data_str.split(',')				
				product.yieldDatas.create(
					yield_date: data[0],
					yield_data: data[1],
					nh_data: data[2])				
			rescue Exception => e
				p e.inspect
				p '8' * 40
			end
		end				
	end

	def update_recent_yield product, yield_data
		yield_arr = yield_data.split(',')
		begin
			if product.update_attributes(yield_date: yield_arr[0],
							  				yield_data: yield_arr[1],				
								 			nh_data: yield_arr[2])	
				puts "ok" << ('-' * 50)
			else
				puts ('*' * 40) << " ID:#{product.id} Name:#{product.product_name}"
			end
		rescue ActiveRecord::RecordInvalid => invalid
			p invalid.record.errors
			p '+' * 40
		end
	end
end