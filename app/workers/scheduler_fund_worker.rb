class SchedulerFundWorker
	include Sidekiq::Worker

	def perform
		Product.all.each do |product|
			product.fetch
		end
	end
end